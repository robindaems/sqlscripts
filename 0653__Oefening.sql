USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration2`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetAlbumDuration2` (in album int,out totalDuration int)
	sql security invoker
BEGIN
	declare songDuration tinyint unsigned default 0;
	declare albumDuration smallint unsigned default 0;
	declare totalDuration smallint unsigned default 0;
	declare albumLength cursor for select Lengte from Liedjes
	where album = Liedjes.Albums_Id;
	open albumLength;
	getLength: LOOP
	fetch albumLength into songDuration;
	set totalDuration = totalDuration + songDuration;
	select totalDuration;
	end loop getLength;
	close albumLength;
END$$

DELIMITER ;

