use ModernWays;
-- Langere code
-- select Artiesten.Naam AS Artiest,Liedjes.Titel AS Titel
-- from Artiesten inner join Liedjes
-- on Artiesten.id = Artiesten_id;
-- korte code
select Naam,Titel
from Liedjes inner join Artiesten on Liedjes.Artiesten_id = Artiesten.id;