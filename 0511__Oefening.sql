USE ModernWays;
INSERT INTO Huisdieren (
	Naam,
    Leeftijd,
    Soort,
    Baasje
)
VALUES
('Misty','6','hond','Vincent'),
('Ming','8','hond','Christiane'),
('Bientje','6','kat','Esther'),
('Flip','75','papegaai','Jommeke'),
('Berto','1','papegaai','Villads'),
('Ming','7','kat','Bert'),
('Suerta','2','hond','Thaïs'),
('Фёдор','1','hond','Lyssa');
	