use ModernWays;
select Id from Studenten
inner join Evaluaties on Studenten.Id = Studenten_Id
group by Studenten.Id
having avg(Cijfer) > (select avg(Cijfer) from Evaluaties);
/*
De oplossing zonder join,wist niet of die tips bij de oplossing hoorde of enkel tips waren om het zonder join op te lossen.
-------------------------------------
select Studenten_Id from Evaluaties
group by Studenten_Id
having avg(Cijfer) > (select avg(Cijfer) from Evaluaties);
*/
