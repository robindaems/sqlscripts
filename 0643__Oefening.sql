USE `aptunes`;
DROP procedure IF EXISTS `CreateAndReleaseAlbum`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateAndReleaseAlbum`(in titel varchar(100),in bands_Id int)
BEGIN
	start transaction;
	insert into Albums (Titel)  values (titel);
    insert into AlbumReleases (Bands_Id,Albums_Id) values (bands_Id,last_insert_id());
    commit;
END$$

DELIMITER ;