use ModernWays;
alter view AuteursBoeken
as
select concat (Personen.Voornaam,' ',Personen.Familienaam) AS Auteur, Boeken.Titel , Boeken.Id as 'Boeken_Id'
from Publicaties
inner join Boeken on Boeken.Id = Publicaties.Boeken_Id
inner join Personen on Personen.Id = Publicaties.Personen_Id