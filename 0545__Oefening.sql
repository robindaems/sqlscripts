use ModernWays;
select Artiest, sum(Aantalbeluisteringen) as Totaal_Aantal_Beluisteringen
from Liedjes
where length(Artiest)>=10
group by Artiest
having sum(Aantalbeluisteringen) > 100;