use ModernWays;
select Games.Titel,
coalesce (Releases.Games_Id,'Geen platformen gekend') as 'Naam'
from Games
left join Releases on Releases.Games_Id = Games.Id where Releases.Platformen_Id is null
UNION ALL
select coalesce (Releases.Platformen_Id, 'Geen games gekend') as 'Titel', Platformen.Naam
from Platformen
left join Releases on Releases.Platformen_Id = Platformen.Id where Releases.Games_Id is null