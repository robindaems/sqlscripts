use ModernWays;
select Leden.Voornaam, Taken.Omschrijving
from Taken
left join Leden
on Taken.Id = Leden.Id
where Leden.Id is null;