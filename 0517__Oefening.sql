USE ModernWays;
-- kan ook (Jaar -1 of 4) ='1'
-- -1 begint vanachter en de 1 na de komma wijst aan tot welk cijfer het controleert.
-- bv (Jaar 3,2) = '71' zou beginnen controleren vanaf het 3de cijfer en tot het 4de cijfer controleren en alleen de jaartallen eindigend op 71 tonen
SELECT * FROM Liedjes WHERE substring(Jaar, -1,1) = '1' ORDER BY Jaar;