USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumRelease`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumRelease`()
BEGIN
	declare numberOfAlbums int default 0;
    declare numberOfBands int default 0;
    declare randomAlbumId int default 0;
    declare randomBandId int default 0;
    select count(*) into numberOfAlbums from Albums;
    select count(*) into numberOfBands from Bands;
	set randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
	set randomBandId = FLOOR(RAND() * numberOfBands) + 1;
    if (randomBandId, randomAlbumId) not in (select * from Albumreleases) then 
    insert into Albumreleases(Bands_Id, Albums_Id)
    values (randomBandId, randomAlbumId);
    end if;
END$$

DELIMITER ;