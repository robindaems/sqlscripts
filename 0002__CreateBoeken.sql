USE ModernWays;
-- dit is mijn eerste tabel
CREATE TABLE Boeken  (
Voornaam VARCHAR(50) CHAR SET utf8mb4,
Familienaam VARCHAR(80) CHAR SET utf8mb4,
Titel VARCHAR(255) CHAR SET utf8mb4,
Stad VARCHAR(50) CHAR SET utf8mb4,
-- alleen het jaartal, geen datetime
-- omdat de kleinste datum daarin 1753 is
-- varchar omdat we ook jaartellen hebben kleiner dan 100
Verschijningsjaar VARCHAR(4),
Uitgeverij VARCHAR(80) CHAR SET utf8mb4,
Herdruk VARCHAR(4),
Commentaar VARCHAR(1000)
);
