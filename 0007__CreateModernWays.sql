CREATE DATABASE ModernWays;
USE ModernWays;
CREATE TABLE Boeken (
	Voornaam VARCHAR(50) CHARSET utf8mb4,
    Familienaam VARCHAR(80) CHARSET utf8mb4 NOT NULL,
    Titel VARCHAR(255) CHARSET utf8mb4,
    Stad VARCHAR(50) CHARSET utf8mb4,
    Verschijningsjaar VARCHAR(4),
    Uitgeverij VARCHAR(80) CHARSET utf8mb4,
    Herdruk VARCHAR(4),
    Commentaar VARCHAR(150) CHARSET utf8mb4
    );
    
