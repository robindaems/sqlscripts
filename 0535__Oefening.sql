use ModernWays;
set SQL_SAFE_UPDATES = 0;
-- like '%9%' of '__9_' of '189_' zou ook kunnen maar dit zou ook boeken van het jaar 1890 enz verwijderen 
delete from Boeken where Verschijningsjaar like '199_';
set sql_safe_updates = 1;