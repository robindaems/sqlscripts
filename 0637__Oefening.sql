use ModernWays;
/* 
Aanmaken van een tabel genaamd 'Personen' met enkel de voornaam en familienaam van de auteurs
*/
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);

/* 
Voornamen en familienamen van de auteurs in de tabel 'Boeken' in de tabel 'Personen' steken (distinct zodat hij ze elke keer maar 1 keer neemt)
*/
insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;
   
/* 
Controleren of er dubbele namen instaan
*/
select Voornaam, Familienaam from Personen
    order by Voornaam, Familienaam;
    
/* 
Toevoegen van overige kolommen voor de tabel 'Personen' + de primary key (id).
*/
alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null); 

/* 
Linken van 'Boeken' met 'Personen' door middel van een foreign key kolom.
*/
alter table Boeken add Personen_Id int null;

/* 
Hier vul ik de foreign key kolom om 'Boeken' met 'Personen' te linken verder in door middel van een update statement.
Hier zet hij de Foreign Key Kolom (Boeken.Personen_Id) id gelijk aan Personen.id als de Voornaam en Familienaam kolommen overeenkomen.
*/    
set sql_safe_updates = 0;    
update Boeken cross join Personen
    set Boeken.Personen_Id = Personen.Id   
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;
set sql_safe_updates = 1;    

/* 
Wijzigen van Personen_Id (foreign key kolom) zodat hij verplicht is nu.
*/
alter table Boeken change Personen_Id Personen_Id int not null;

/* 
Verwijderen van de dubelle kolommen in de tabel 'Boeken'.
*/
alter table Boeken drop column Voornaam,
    drop column Familienaam;
    
/* 
Foreign key constraint op de kolom Personen_Id toepassen.
*/
alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);
