USE `aptunes`;
DROP procedure IF EXISTS `CleanupOldMemberships`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CleanupOldMemberships`(in someDate date,out numberCleaned int)
BEGIN
	start transaction;
    select count(*) into totalMembership from Lidmaatschappen;
    set sql_safe_updates = 0;
    delete from Lidmaatschappen
    where Einddatum < someDate;
    set sql_safe_updates = 1;
    set numberCleaned = totalMembership- (select count(*) from Lidmaatschappen);
    commit;
END$$

DELIMITER ;