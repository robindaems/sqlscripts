USE `aptunes`;
DROP procedure IF EXISTS `GetLiedjes`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetLiedjes`(in nameLiedjes varchar(50))
BEGIN
    select Titel from Liedjes
    where Titel like CONCAT('%',nameLiedjes,'%');
END$$

DELIMITER ;