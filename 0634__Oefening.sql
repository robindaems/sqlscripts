use ModernWays;
/*
select min(Gemiddelde) as Gemiddelde from
(select avg(Cijfer) as Gemiddelde from Evaluaties group by Studenten_Id) as Gemiddelde;
*/
select distinct avg(Cijfer) as Gemiddelde from Evaluaties
group by Studenten_Id
having (avg(Cijfer) <= all (select avg(Cijfer) from Evaluaties group by Studenten_Id));