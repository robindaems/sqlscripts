use ModernWays;
alter table Baasjes
add column Huisdieren_id int,
add constraint fk_Baasjes_Baasje
	foreign key (Huisdieren_id)
    references Huisdieren(id);
