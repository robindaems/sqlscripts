USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleases`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleases` (in extraReleases int)
BEGIN
	declare counter int default 0;
    declare success bool;
    repeat call MockAlbumReleaseWithSucces(success);
    if success = 1 then set counter = counter +1;
    end if;
    until counter = extraReleases
    end repeat;
END$$

DELIMITER ;