/* show indexes from Genres -- Kijken of het unique moet zijn of niet en wat de perfecte prefix is
select count(distinct Naam) from Liedjes;
select count(distinct left(Naam,2)) from Genres; */
create index NaamIdx
on Genres(Naam(2));
-- toont alle geldige combinaties van liedjestitels en genres
select Titel, Naam
from Liedjesgenres inner join Liedjes
on Liedjesgenres.Liedjes_Id = Liedjes.Id
inner join Genres
on Liedjesgenres.Genres_Id = Genres.Id
where Naam = 'Rock';