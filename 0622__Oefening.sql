-- Uitzoeken van perfecte prefix
-- select count(distinct Voornaam)
-- from Muzikanten;
-- select count(distinct left(Voornaam,9))
-- from Muzikanten;
-- select count(distinct Familienaam)
-- from Muzikanten;
-- select count(distinct left(Familienaam,9))
-- from Muzikanten;
create index VoornaamFamilienaamIdx
on Muzikanten(Voornaam(9),Familienaam(9));

