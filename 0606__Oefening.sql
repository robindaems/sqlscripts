use ModernWays;
select Leden.Voornaam, Boeken.Titel
from Uitleningen
inner join Leden on Uitleningen.Leden_Id = Leden.Id
inner join Boeken on Uitleningen.Boeken_Id = Boeken.Id;