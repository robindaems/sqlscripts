use ModernWays;
alter table Liedjes
add column Artiesten_id int,
add constraint fk_Liedjes_Artiesten foreign key (Artiesten_id)
references Artiesten(id);
