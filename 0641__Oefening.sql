USE `aptunes`;
DROP procedure IF EXISTS `NumberOfGenres`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `NumberOfGenres`(out totalGenres tinyint)
BEGIN
	select count(*)
    into totalGenres
    from Genres;
END$$

DELIMITER ;