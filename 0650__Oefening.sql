USE `aptunes`;
DROP procedure IF EXISTS `DangerousInsertAlbumreleases`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DangerousInsertAlbumreleases`()
BEGIN
	declare numberOfAlbums int default 0; 
	declare numberOfBands int default 0;
	declare randomAlbumId int default 0;
	declare randomBandId int default 0;
	declare amount int default 1;
    declare errorHandling int;
	declare exit handler for sqlexception
	begin rollback;
	select 'Nieuwe releases konden niet worden toegevoegd.';
	end;
	set errorHandling = floor(rand() * 3)  + 1;
	select errorHandling;
	start transaction;
	while amount <= 3 do
	if (amount > 1 and errorHandling = 1) then
	signal sqlstate '45000';
	end if;
	select count(*) into numberOfAlbums from albums;
	select count(*) into numberOfBands from bands;
	set randomAlbumId = floor(rand() * numberOfAlbums) + 1;
	set randomBandId = floor(rand()*numberOfBands) + 1;
	if ((select count(*) from Albumreleases where Bands_Id = randomBandId) < 1
	and
	(select count(*) from Albumreleases where Albums_Id = randomAlbumId) < 1 ) 
	then select concat(amount, ' albums toegevoegd');
	set amount = amount + 1;
	insert into Albumreleases (Bands_Id,Albums_Id) values (randomBandId,randomAlbumId);
	else signal sqlstate '45000';
	end if;
	end while;
	commit;
END$$

DELIMITER ;
