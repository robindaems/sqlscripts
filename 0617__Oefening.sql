use ModernWays;
create view AteursBoekenRating as
select concat (Personen.Voornaam,' ',Personen.Familienaam) AS 'Auteur',Titel,Rating
from Boeken
inner join Publicaties on Boeken.Id = Publicaties.Boeken_Id
inner join Personen on Publicaties.Personen_Id = Personen.Id
inner join GemiddeldeRatings on Boeken.Id = GemiddeldeRatings.Boeken_Id