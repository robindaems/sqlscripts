USE ModernWays;
/*
SELECT AVG(Leeftijd)
FROM Honden
GROUP BY Geslacht
HAVING Geslacht = 'mannelijk';
*/
select Geslacht
from Honden
group by Geslacht
having avg(leeftijd) > 4;