USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleasesLoop`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleasesLoop` (in extraReleases int)
BEGIN
	declare counter int default 0;
    declare success bool;
    mockAlbumLoop : loop
    call MockAlbumReleaseWithSucces(success);
    if success = 1 then
    set counter = counter + 1;
    end if;
    if counter = extraReleases then
    leave mockAlbumLoop;
    end if;
    end loop;     
END$$

DELIMITER ;