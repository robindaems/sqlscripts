USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DemonstrateHandlerOrder`()
BEGIN
    declare errorHandling int;
    declare exit handler for SQLSTATE '45002' select 'State 45002 opgevangen. Geen probleem' /* ErrorCode , moet dit erachter ? */;
	declare exit handler for sqlexception  select 'Een algemene fout opgevangen.' /* Message , moet dit erachter ? */;
    set errorHandling = floor(rand() * 3) + 1;
    if errorHandling = 1 
    then signal sqlstate '45001'; 
    elseif errorHandling = 2
    then signal sqlstate '45002';
    else signal sqlstate '45003';
    end if;
END$$

DELIMITER ;