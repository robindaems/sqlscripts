-- ERM Oefening
use ModernWays;
drop table if exists Student;
drop table if exists Opleiding;
drop table if exists Vak;
drop table if exists Lector;
create table Student(
	Studentennummer int auto_increment primary key,
    Voornaam varchar(100) charset utf8mb4 not null,
    Famillienaam varchar(100) charset utf8mb4 not null
    );
create table Opleiding(
	Naam varchar(100) charset utf8mb4  primary key
    );
create table Vak(
	Naam varchar(100) charset utf8mb4  primary key
    );
    create table Lector(
		Personeelsnummer int auto_increment primary key,
        Voornaam varchar(100) charset utf8mb4 not null,
        Famillienaam varchar(100) charset utf8mb4 not null
        );
alter table Student 
add column Semester tinyint unsigned,
add column Opleiding_Naam varchar(100) charset utf8mb4,
add constraint fk_Student_Opleiding foreign key (Opleiding_Naam)
references Opleiding(Naam)
;

